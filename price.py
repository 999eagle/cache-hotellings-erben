import math
from operator import add


def single_net_price(dx, dy, factory_price, transport_cost_contribution):
    """
    net price for delivering to a factory with distance (`dx`, `dy`), price `factory_price` and given `transport_cost_contribution`
    """
    distance_sqr = dx * dx + dy * dy
    net_price = factory_price / \
        (1 + 0.1 * distance_sqr * (1 - transport_cost_contribution))
    transport_cost = 0.1 * distance_sqr * net_price
    return (net_price, transport_cost)


def all_net_prices(x, y, factory_prices, transport_cost_contributions):
    """
    net prices for field at (`x`, `y`) with given factory prices and transport cost contributions
    """
    np_a = single_net_price(
        x, y, factory_prices[0], transport_cost_contributions[0])
    np_b = single_net_price(
        x, 25 - y, factory_prices[1], transport_cost_contributions[1])
    np_c = single_net_price(
        25 - x, y, factory_prices[2], transport_cost_contributions[2])
    np_d = single_net_price(
        25 - x, 25 - y, factory_prices[3], transport_cost_contributions[3])
    return [np_a, np_b, np_c, np_d]


def delivery_to(x, y, factory_prices, transport_cost_contributions):
    """
    where to sell from field (`x`, `y`) with given factory prices and transport cost contribution
    """
    net_prices_costs = all_net_prices(
        x, y, factory_prices, transport_cost_contributions)
    net_prices = [p[0] for p in net_prices_costs]
    transport_costs = [p[1] for p in net_prices_costs]
    max_price = max(net_prices)
    deliver_to = [(0 if x < max_price else 1) for x in net_prices]
    num_factories = sum(deliver_to)
    p = max_price / num_factories
    delivery = [x * p for x in deliver_to]
    transport_costs = [x / num_factories for x in transport_costs]
    return list(zip(delivery, transport_costs))


def factory_profit(factory_prices, transport_cost_contributions=[0, 0, 0, 0]):
    profits = [0, 0, 0, 0]
    for x in range(1, 25):
        for y in range(1, 25):
            delivery = delivery_to(x, y, factory_prices,
                                   transport_cost_contributions)
            for i in range(0, 4):
                if delivery[i][0] > 0:
                    profits[i] += delivery[i][0] * \
                        (5.0 - factory_prices[i]) - delivery[i][1] * \
                        transport_cost_contributions[i]
    return profits
