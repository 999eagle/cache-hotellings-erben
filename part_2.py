#!/usr/bin/env python

import price

if __name__ == "__main__":
    max_profit = 0
    max_profit_price = 0
    for p in range(0, 501):
        p = p / 100.0
        factory_prices = [p, p, p, p]
        profits = price.factory_profit(factory_prices)
        if profits[0] > max_profit:
            max_profit_price = factory_prices[0]
            max_profit = profits[0]
    print(f'profit {max_profit} at {max_profit_price}')
