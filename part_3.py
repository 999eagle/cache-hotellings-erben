#!/usr/bin/env python

import price


def main():
    # factory_prices = [2.5, 2.5, 2.5, 2.5]
    # contributions = [0.9, 0, 0, 0]
    # profits = price.factory_profit(factory_prices, contributions)
    # print(f'profits: {profits}')
    # return
    max_profit = 0
    max_profit_price = 0
    max_profit_contribution = 0
    max_profit_profits = 0
    for c in range(0, 11):
        c = c / 10.0
        for p in range(0, 501):
            p = p / 100.0
            factory_prices = [p, 4.0, 4.0, 4.0]
            contributions = [c, 0.0, 0.0, 0.0]
            profits = price.factory_profit(factory_prices, contributions)
            if profits[0] > max_profit:
                max_profit_price = factory_prices
                max_profit_contribution = contributions
                max_profit_profits = profits
                max_profit = profits[0]
    print(
        f'profit {max_profit_profits} at {max_profit_price} and {max_profit_contribution}')


def test():
    max_profit = 0
    max_profit_price = 0
    max_profit_contribution = 0
    max_profit_profits = 0
    c_2 = 0.2
    p_2 = 2.3
    for c in range(0, 11):
        c = c / 10.0
        for p in range(15, 31):
            p = p / 10.0
            factory_prices = [p, p_2, p_2, p_2]
            contributions = [c, c_2, c_2, c_2]
            profits = price.factory_profit(factory_prices, contributions)
            if profits[0] > max_profit:
                max_profit_price = factory_prices
                max_profit_contribution = contributions
                max_profit_profits = profits
                max_profit = profits[0]
    print(
        f'profit {max_profit_profits} at {max_profit_price} and {max_profit_contribution}')


if __name__ == "__main__":
    main()
