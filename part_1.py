#!/usr/bin/env python

import price

if __name__ == "__main__":
    max_profit = 0
    max_profit_price = 0
    for p in range(250, 350):
        factory_prices = [p / 100.0, 4.0, 4.0, 4.0]
        profits = price.factory_profit(factory_prices)
        if profits[0] > max_profit:
            max_profit_price = factory_prices[0]
            max_profit = profits[0]
    print(f'profit {max_profit} at {max_profit_price}')
