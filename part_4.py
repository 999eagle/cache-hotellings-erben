#!/usr/bin/env python

import price
import random


def optimise(base_price, base_contrib, opt_idx):
    max_profit = 0
    max_profit_price = [0.0, 0.0, 0.0, 0.0]
    max_profit_contribution = [0.0, 0.0, 0.0, 0.0]
    max_profit_profits = [0.0, 0.0, 0.0, 0.0]
    for c in range(0, 11):
        c = c / 10.0
        for p in range(150, 301, 5):
            p = p / 100.0
            factory_prices = list(base_price)
            contributions = list(base_contrib)
            factory_prices[opt_idx] = p
            contributions[opt_idx] = c
            profits = price.factory_profit(factory_prices, contributions)
            if profits[opt_idx] > max_profit:
                max_profit_price = factory_prices
                max_profit_contribution = contributions
                max_profit_profits = profits
                max_profit = profits[opt_idx]
    return (max_profit_profits, max_profit_price, max_profit_contribution)


def main():
    epsilon = 0.1
    base_price = [4.0, 4.0, 4.0, 4.0]
    base_contrib = [0.0, 0.0, 0.0, 0.0]
    profits = [0.0, 0.0, 0.0, 0.0]
    while True:
        next_prices = list(base_price)
        next_contrib = list(base_contrib)
        for i in range(0, 4):
            (profits, prices, contributions) = optimise(
                base_price, base_contrib, i)
            next_prices[i] = prices[i]
            next_contrib[i] = contributions[i]
        base_price = next_prices
        base_contrib = next_contrib
        profits = price.factory_profit(base_price, base_contrib)
        print(
            f'profits {profits} at {base_price} and {base_contrib}')


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
